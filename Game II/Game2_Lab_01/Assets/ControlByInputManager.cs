﻿using UnityEngine;

 public class ControlByInputManager : MonoBehaviour 
 {
 public float _horizontalMovementScale=0.5f;
 public float _verticalMovementScale=0.5f;

// Use this for initialization
 void Start () {

 }
 // Update is called once per frame
  void Update () {
 this.transform.Translate(
 new Vector3(Input.GetAxisRaw("Horizontal")*_horizontalMovementScale ,
 Input.GetAxisRaw("Vertical")*_verticalMovementScale ,
 0));
 }
 }
