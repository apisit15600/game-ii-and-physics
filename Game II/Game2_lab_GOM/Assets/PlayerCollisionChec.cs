﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionChec : MonoBehaviour {
     
      [SerializeField]
    GameObject _Effect;
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Item") {
         GameObject go = Instantiate (_Effect,collision.gameObject.transform.position,Quaternion.identity); 
            Destroy (collision.gameObject);
             Destroy (go,2.0f);  
        } else if (collision.gameObject.tag == "Obstacle") {
            Destroy (collision.gameObject, 0.5f);
        }
    }
}